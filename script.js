function addition() {
    var input1 = parseFloat(document.getElementById('input1').value);
    var input2 = parseFloat(document.getElementById('input2').value);
    var result = input1 + input2;
    document.getElementById('result').value = result;
}
function subtract() {
    var input1 = parseFloat(document.getElementById('input1').value);
    var input2 = parseFloat(document.getElementById('input2').value);
    var result = input1 - input2;
    document.getElementById('result').value = result;
}
function multiply() {
    var input1 = parseFloat(document.getElementById('input1').value);
    var input2 = parseFloat(document.getElementById('input2').value);
    var result = input1 * input2;
    document.getElementById('result').value = result;
}

function division() {
    var input1 = parseFloat(document.getElementById('input1').value);
    var input2 = parseFloat(document.getElementById('input2').value);
    if(input2 == 0){
        document.getElementById('result').value = 'Cannot divide by 0';
    }
    else {
        var result = input1 / input2;
        document.getElementById('result').value = result;
    }
}